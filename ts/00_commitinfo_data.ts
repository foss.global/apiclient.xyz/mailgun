/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@mojoio/mailgun',
  version: '2.0.1',
  description: 'an api abstraction package for mailgun'
}
